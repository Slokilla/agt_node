const http = require('http');
const fs = require('fs');
const express = require('express');
const session = require("express-session")({
    secret: "my-secret",
    resave: true,
    saveUninitialized: true
});
const sharedsession = require("express-socket.io-session");
const app = express();
app.use(session);
// Chargement du fichier index.html affiché au client
const server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});

// Chargement de socket.io
const io = require('socket.io').listen(server);

io.use(sharedsession(session, {
    autoSave:true
}));

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {

    socket.emit('connected');

    socket.on('new_user', username => {
        socket.handshake.session.username = username;
        socket.handshake.session.save();
        socket.broadcast.emit("new_user", username);
    })

    socket.on('message', msg => {
        const sender = socket.handshake.session.username;
        socket.broadcast.emit("message", { sender, content:msg.content});
    })

    socket.on('disconnect', () => {
        let sender = socket.handshake.session.username;
        console.log(sender);
        socket.broadcast.emit("disconnected_user", sender);
        //socket.emit("disconnected");
    });
});


server.listen(8080);