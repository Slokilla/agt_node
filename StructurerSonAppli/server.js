var express = require('express');

var app = express();

app.get('/etage/:etagenum/chambre', function(req, res) {
    res.render('chambre.twig', {etage: req.params.etagenum});
});

app.listen(8080);