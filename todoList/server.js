const session = require('express-session');
const twig = require('twig');
const express = require('express');

const app = express();
//Le parser pour analyser les requête effectuées avec la méthode POST
const urlencodedParser = express.urlencoded({ extended: false, type: "*/*" })
const sess = {
    secret: 's3Cur3',
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 60000}
}

//Tout les exemples de la doc utilisent cette ligne,
//Je la met en attendant de me renseigner dessus.
app.set('trust proxy', 1) // trust first proxy

app.use(session(sess))

/**
 * @Route : /
 * @Get {
 *      Appelée par défaut par le navigateur.
 * }
 * @Post {
 *     Appelée quand l'utilisateur ajoute une tâche.
 *     @PARAM addedTask une chaine de caractère correspondant à la tâche ajoutée
 *     @THROWS Error si il y a un problême dans la requête.
 * }
 */
.route('/')
    .get((req, res, next) => {
        if (!req.session.todolist) {
            req.session.todolist = {};
        }
        res.render('base.twig', {todolist: req.session.todolist});
    })
    .post(urlencodedParser, (req, res, next) => {
        if (req.body.addedTask) {
            if (!req.session.todolist) {
                req.session.todolist = {};
            }
            req.session.todolist[req.body.addedTask] = false;
            res.render('base.twig', {todolist: req.session.todolist});
        } else {
            throw new Error("Requête incomprise");
        }
    });

/**
 * @Route : /delete
 * @Get {
 *      Appelée par l'utilisateur quand il clique sur une croix de tâche
 *      Puisque l'application ne sert qu'à cela, la tâche à supprimer et passée via l'url,
 *      mais je suis bien conscient que ce n'est pas la manière la plus propre de faire.
 *      La tâche à supprimer n'est pas réellement supprimée, elle est passée à true dans la session,
 *      dans l'optique d'améliorer l'appli et de faire un historique des tâches accomplies.
 *      @PARAM toDel une chaine de caractères correspondant à la tâche à supprimer
 * }
 */
app.get("/delete", urlencodedParser,(req, res, next) => {
    if (req.query.toDel) {
        if (req.session.todolist) {
            req.session.todolist[req.query.toDel] = true;
            res.redirect("/");
        }
    }
})

/**
 * @Route : /reinit
 * @Get {
 *      Implémentée pour le debug, permet de réinitialiser la session
 * }
 */
.get('/reinit', (req, res, next) => {
    req.session.destroy();
    res.redirect("/");
})

/**
 * @Route : Toute route non gérée par l'application.
 * @AllMethods {
 *     Redirige à l'accueil.
 * }
 */
.use((req, res, next) => {
    res.redirect("/");
});

console.log("listening ...");
app.listen("8080");
